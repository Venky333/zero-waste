import { Injectable } from '@angular/core';
import { RestServiceService } from './rest-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContainerdetailService {

  constructor(private restService: RestServiceService) { }

  public getContainerHistory(id: string): Observable<any> {
     return this.restService.getContainerHistory(id);
  }

  public getContainerStatus(id: string): Observable<any> {
    return this.restService.getContainerStatus(id);
  }
}
