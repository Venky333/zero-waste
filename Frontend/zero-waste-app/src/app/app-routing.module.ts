import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QrscanComponent } from './qrscan/qrscan.component';
import { SelectProductComponent } from './select-product/select-product.component';
import { DispenseComponent } from './dispense/dispense.component';
import { ContainerdetailComponent } from './containerdetail/containerdetail.component';
import { DatavisualisationComponent } from './datavisualisation/datavisualisation.component';

const routes: Routes = [
  {path: '', component: QrscanComponent},
  {path: 'select', component: SelectProductComponent},
  {path: 'dispense', component: DispenseComponent},
  {path: 'containerdetail/:qrcode', component: ContainerdetailComponent},
  {path: 'datavis', component: DatavisualisationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
