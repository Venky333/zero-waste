import { TestBed } from '@angular/core/testing';

import { ContainerdetailService } from './containerdetail.service';

describe('ContainerdetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContainerdetailService = TestBed.get(ContainerdetailService);
    expect(service).toBeTruthy();
  });
});
