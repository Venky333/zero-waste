import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QrscanComponent } from './qrscan/qrscan.component';
import { SelectProductComponent } from './select-product/select-product.component';
import { DispenseComponent } from './dispense/dispense.component';
import { ZXingScannerModule, ZXingScannerComponent } from '@zxing/ngx-scanner';
import { ContainerdetailComponent } from './containerdetail/containerdetail.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './tokeninterceptor';
import { DatavisualisationComponent } from './datavisualisation/datavisualisation.component';

@NgModule({
  declarations: [
    AppComponent,
    QrscanComponent,
    SelectProductComponent,
    DispenseComponent,
    ContainerdetailComponent,
    DatavisualisationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ZXingScannerModule.forRoot()
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})

export class AppModule { }
