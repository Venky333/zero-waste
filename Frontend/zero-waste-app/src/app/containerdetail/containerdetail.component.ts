import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Observable, empty } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ContainerdetailService } from '../containerdetail.service';
import { ContainerHistoryDetail } from '../container-history-detail';
import { StatusserviceService } from '../statusservice.service';

@Component({
  selector: 'app-containerdetail',
  templateUrl: './containerdetail.component.html',
  styleUrls: ['./containerdetail.component.scss']
})
export class ContainerdetailComponent implements OnInit {

  private containerHistory: Observable<any>;
  private status:any;
  private history: ContainerHistoryDetail[];
  constructor(private route: ActivatedRoute,
    private router: Router, private containerDetailService: ContainerdetailService, private statusService: StatusserviceService) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
          this.containerDetailService.getContainerHistory(params.get('qrcode')))).subscribe(
            (next) => {
              console.log(next); 
              this.history = JSON.parse(JSON.stringify(next))
            });
    this.route.paramMap.pipe(switchMap((params: ParamMap) =>
    this.containerDetailService.getContainerStatus(params.get('qrcode')))).subscribe(
      (next) => {
        console.log(next); 
        this.status = JSON.parse(JSON.stringify(next))
      });
  }

}
