import { TestBed } from '@angular/core/testing';

import { StatusserviceService } from './statusservice.service';

describe('StatusserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StatusserviceService = TestBed.get(StatusserviceService);
    expect(service).toBeTruthy();
  });
});
