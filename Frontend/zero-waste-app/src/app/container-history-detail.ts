export class ContainerHistoryDetail {
    "id": string;
    "type": string;
    "owner": string;
    "status": string;
    "manufacturer": string;
    "dateOfManufacturing": Date
}
