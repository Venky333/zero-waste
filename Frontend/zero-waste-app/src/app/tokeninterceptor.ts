import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { StatusserviceService } from './statusservice.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

private token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ikhlcm8iLCJpYXQiOjE1NTc0OTc0NTF9.MV1xOeVPsVnwBS8U4VDmwGgMwg-XvesTiYQsQ6YqS2s";

  constructor(public statusService:StatusserviceService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + this.token) });
    request = request.clone({ headers: request.headers.set('Content-Type', 'application/json'), withCredentials: true});
    console.log(request);
    
    return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('event--->>>', event);
                    // this.errorDialogService.openDialog(event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                let data = {};
                data = {
                    reason: error && error.error.reason ? error.error.reason : '',
                    status: error.status
                };
                this.statusService.status = "Not reusable container, please return this container to be cleaned";
                return throwError(error);
            }));;
  }
}