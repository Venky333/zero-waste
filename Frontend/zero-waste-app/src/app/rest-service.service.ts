import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RestServiceService {

  private backendUrl = "http://192.168.0.59:3000/";

  constructor(private httpClient: HttpClient) { }

  public getContainerHistory(containerid: string): Observable<any>{
    let data = {
      "id": containerid
    }
    return this.httpClient.post(this.backendUrl + 'chain/query/getHistories',data);
  
  }

  public getContainerStatus(containerid: string): Observable<any>{
    let data = {
      "id": containerid,
      "type": "food",
      "owner": "cleaningOrganisation_1",
      "status": "Cleaned",
      "manufacturer": "manufacturer_1",
      "dateOfManufacturing": "2019-05-17T09:36:48.527Z"
   }
    return this.httpClient.post(this.backendUrl + 'chain/invoke/putContainer',data);
  
  }

  //public applyOrderToBlockChain(containerid: string, orderid:string, )

}
