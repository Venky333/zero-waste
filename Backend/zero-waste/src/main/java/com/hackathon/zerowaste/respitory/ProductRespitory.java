package com.hackathon.zerowaste.respitory;


import com.hackathon.zerowaste.models.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRespitory extends CrudRepository<Product, Integer> {
}
