package com.hackathon.zerowaste.models;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import javax.persistence.*;

@Entity
@Table(schema = "Order_Jobs")
public class OrderJobs {
    String product;
    int status=1;
    double quantity;
    static int orderCounter=0;
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="order_number")
    int orderNumber;

//    public OrderJobs(String product, double quantity) {
//        this.product = product;
//        this.status = 1;
//        this.quantity = quantity;
//    }

    public OrderJobs() {
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public static int getOrderCounter() {
        return orderCounter;
    }

    public static void setOrderCounter(int orderCounter) {
        OrderJobs.orderCounter = orderCounter;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getOrderNumber() {
        return orderNumber;
    }


}

