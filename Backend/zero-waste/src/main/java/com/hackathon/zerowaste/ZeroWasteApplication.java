package com.hackathon.zerowaste;

import com.hackathon.zerowaste.controller.mqttSubscriberController;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.net.URISyntaxException;

@SpringBootApplication
@EnableJpaRepositories
public class ZeroWasteApplication {

	public static void main(String[] args) {




		SpringApplication.run(ZeroWasteApplication.class, args);
	}

}
