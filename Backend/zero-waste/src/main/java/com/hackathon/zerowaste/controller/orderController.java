package com.hackathon.zerowaste.controller;

import com.hackathon.zerowaste.models.OrderJobs;
import com.hackathon.zerowaste.respitory.OrderRespitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class orderController {
    @Autowired(required = true)
    private OrderRespitory orderRepository;
    //HashMap<Integer,OrderJobs> orderBook= new HashMap<Integer, OrderJobs>();
    @GetMapping("/order")
    public OrderJobs getOrder(int orderNumber){
        return orderRepository.findById(orderNumber).get();
    }

    @PostMapping("/order")
    public OrderJobs setOrder(OrderJobs orderJobs){
        orderRepository.save(orderJobs);
        //orderBook.put(orderJobs.getOrderNumber(),orderJobs);
        mqttPublishController mqtt = new mqttPublishController(orderJobs);
        mqtt.publish();
        return orderJobs;
    }
}
