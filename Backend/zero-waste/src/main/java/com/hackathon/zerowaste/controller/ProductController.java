package com.hackathon.zerowaste.controller;


import com.hackathon.zerowaste.models.Product;
import com.hackathon.zerowaste.respitory.ProductRespitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class ProductController {
    @Autowired(required = true)
    private ProductRespitory productRespitory;
    @GetMapping("/product")
    public Iterable<Product> getProduct(){
        return productRespitory.findAll();
    }
    @GetMapping("/product/{id}")
    public Product getProduct(@PathVariable("id") int productNumber){
        return productRespitory.findById(productNumber).get();
    }
}
