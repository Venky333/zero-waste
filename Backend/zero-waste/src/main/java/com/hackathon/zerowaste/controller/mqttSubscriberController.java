package com.hackathon.zerowaste.controller;

import com.hackathon.zerowaste.models.OrderJobs;
import com.hackathon.zerowaste.respitory.OrderRespitory;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Component
public class mqttSubscriberController implements MqttCallback {

    private final int qos = 0;
    private String topic = "dispenser/2";
    private MqttClient client;
    @Autowired(required = true)
    private OrderRespitory orderRepository;

   public mqttSubscriberController() throws MqttException,URISyntaxException {
        URI uri=new URI("tcp://broker.hivemq.com:1883");
        String host = String.format("tcp://%s:%d", uri.getHost(), uri.getPort());
        String[] auth = this.getAuth(uri);
        String username = auth[0];
        String password = auth[1];
        String clientId = String.valueOf((int)(Math.random()*10000));
       System.out.println("Subscriber Function Started!!!!!!!");
       System.out.println("clientId: "+clientId);
        if (!uri.getPath().isEmpty()) {
            this.topic = uri.getPath().substring(1);
        }

        MqttConnectOptions conOpt = new MqttConnectOptions();
        conOpt.setCleanSession(true);
        conOpt.setUserName(username);
        conOpt.setPassword(password.toCharArray());

        this.client = new MqttClient(host, clientId, new MemoryPersistence());
        this.client.setCallback(this);
        this.client.connect(conOpt);

        this.client.subscribe(this.topic, qos);
    }

    private String[] getAuth(URI uri) {
        String a = uri.getAuthority();
        String[] first = a.split("@");
        return first[0].split(":");
    }

    public void sendMessage(String payload) throws MqttException {
        MqttMessage message = new MqttMessage(payload.getBytes());
        message.setQos(qos);
        this.client.publish(this.topic, message); // Blocking publish
    }

    /**
     * @see MqttCallback#connectionLost(Throwable)
     */
    public void connectionLost(Throwable cause) {
        System.out.println("Connection lost because: " + cause);
        System.out.println("clientId: "+this.client.getClientId());
        //System.exit(1);
    }

    /**
     * @see MqttCallback#deliveryComplete(IMqttDeliveryToken)
     */
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

    /**
     * @see MqttCallback#messageArrived(String, MqttMessage)
     */
    public void messageArrived(String topic, MqttMessage message) throws MqttException {
        System.out.println("MQTT message received!!!!!");
        String sMessage = new String(message.getPayload(), StandardCharsets.UTF_8);
    try{
        int orderNumber = Integer.parseInt(sMessage);
        System.out.println(String.format("[%s] %s", topic, sMessage));
        OrderJobs orderJob = orderRepository.findById(orderNumber).get();

        orderJob.setStatus(5);
        orderRepository.save(orderJob);
        System.out.println("OrderJob Updated");

    }catch (Exception e){
        System.out.println(e);
    }

    }
}
