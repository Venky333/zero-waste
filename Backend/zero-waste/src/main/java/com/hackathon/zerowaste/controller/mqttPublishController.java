package com.hackathon.zerowaste.controller;

import com.hackathon.zerowaste.models.OrderJobs;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


public class mqttPublishController {
/*    You can access the broker at:

    Broker: broker.hivemq.com
    TCP Port: 1883
    Topic: dispenser/1 to dispense something and topic: dispense/2 to get feedback once it is done.*/
    String topic        = "dispenser/1";
    String content      = "";
    int qos             = 0;
    String broker       = "tcp://broker.hivemq.com:1883";
    String clientId     = "";

    MemoryPersistence persistence = new MemoryPersistence();

    public mqttPublishController( OrderJobs orderJobs) {
        this.content = orderJobs.getOrderNumber()+";"+ orderJobs.getProduct()+";"+ orderJobs.getQuantity();
        this.clientId = String.valueOf((int)(Math.random()*10000)+1);
        System.out.println("clientId: "+clientId);
    }

    //    public void mqttPublishController(OrderJobs order){
//        this.content = order.getOrderNumber()+";"+order.getProduct()+";"+order.getQuantity();
//    }
    public void publish(){
    try {
        MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        System.out.println("Connecting to broker: "+broker);
        sampleClient.connect(connOpts);
        System.out.println("Connected");
        System.out.println("Publishing message: "+content);
        MqttMessage message = new MqttMessage(content.getBytes());
        message.setQos(qos);
        sampleClient.publish(topic, message);
        System.out.println("Message published");
        sampleClient.disconnect();
        System.out.println("Disconnected");

    } catch(MqttException me) {
        System.out.println("reason "+me.getReasonCode());
        System.out.println("msg "+me.getMessage());
        System.out.println("loc "+me.getLocalizedMessage());
        System.out.println("cause "+me.getCause());
        System.out.println("excep "+me);
        me.printStackTrace();
    }
}

}
