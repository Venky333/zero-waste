package com.hackathon.zerowaste.respitory;

import com.hackathon.zerowaste.models.OrderJobs;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


@Repository
public interface OrderRespitory extends CrudRepository<OrderJobs, Integer> {

}
