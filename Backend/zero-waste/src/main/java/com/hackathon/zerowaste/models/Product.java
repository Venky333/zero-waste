package com.hackathon.zerowaste.models;

import javax.persistence.*;

@Entity
@Table(schema = "Product")
public class Product {
        @Id
        @GeneratedValue(strategy= GenerationType.AUTO)
        int id;
        String title;
        String description;
        String url;
        int stock;
        String unitQuantity;
        double productPrize;
        public Product() {
        }

        public int getId() {
                return id;
        }

        public void setId(int id) {
                this.id = id;
        }

        public double getProductPrize() {
                return productPrize;
        }

        public void setProductPrize(double productPrize) {
                this.productPrize = productPrize;
        }

        public String getTitle() {
                return title;
        }

        public void setTitle(String title) {
                this.title = title;
        }

        public String getDescription() {
                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public String getUrl() {
                return url;
        }

        public void setUrl(String url) {
                this.url = url;
        }

        public int getStock() {
                return stock;
        }

        public void setStock(int stock) {
                this.stock = stock;
        }

        public String getUnitQuantity() {
                return unitQuantity;
        }

        public void setUnitQuantity(String unitQuantity) {
                this.unitQuantity = unitQuantity;
        }
}
