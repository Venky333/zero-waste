# hackathon2019-colruyt-zero-waste
Reducing waste...

# Testing frontend
Install the necessary tools
Open the zero-waste-frontend folder in VSCode and start the app with the command 'ng serve' in a terminal
Open http://localhost:4200 in a browser to verify

# Testing backend
Install the necesary tools
Open the project in IntelliJ
Open a terminal and run the commands:

./mvnw package
cd target
java -jar zero-waste-0.0.1-SNAPSHOT.jar

Open http://localhost:8080/actuator in a browser to verify
